//
//  Card.swift
//  CCG
//
//  Created by Brian Broom on 7/8/14.
//  Copyright (c) 2014 Brian Broom. All rights reserved.
//

import Foundation
import SpriteKit

//enum CardNumber :Int {
//    case CreatureWolf = 0,      // 0
//    CreatureBear,               // 1
//    CreatureDragon,             // 2
//    Energy,                     // 3
//    SpellDeathRay,              // 4
//    SpellRabid,                 // 5
//    SpellSleep,                 // 6
//    SpellStoneskin              // 7
//}

class Card : SKSpriteNode {
    
    // card attributes
    
    let cardType :String
    let cardName :RWCardName
    
    let spellDuration = 0
    let spellTargets :String?
    let energyCost = 0
    // base values for card, not modified
    // if you need current values, use attackValue
    // or defenseValue
    let cardAttackValue = 0
    let cardDefenseValue = 0
    var attackValue :Int {
        // get card value and total attached cards
        var total = cardAttackValue
            
            for attachment in attachedCards {
                total += attachment.cardAttackValue
            }
        
        return total
    }
    var defenseValue :Int {
        // get card value and total attached cards
        var total = cardDefenseValue
            
            for attachment in attachedCards {
                total += attachment.cardDefenseValue
            }
            
            return total
    }
    
    // player and scene
    weak var gameScene :RWGameScene? {
        return scene as? RWGameScene
    }
    weak var player :Player?
    
    // textures
    let frontTexture :SKTexture
    let highlightTexture :SKTexture
    let backTexture :SKTexture
    let largeCardFileName :String
    let damageLabel :SKLabelNode
    
    // position
    override var zPosition :CGFloat {
        didSet {
            let topCardZPosition = zPosition
            for (index,attachment) in enumerate(attachedCards) {
                attachment.zPosition = topCardZPosition - CGFloat((index + 1))
            }
        }
    }
    var savedPosition = CGPointZero
    var savedZPosition:CGFloat = 0.0
    var savedZRotation:CGFloat = 0.0

    // this method is to help with positioning of P1 and P2 elements
    // since P2 elements area drawn upside down, moving a card "up" as
    // in attachments means they should move "down" in screen coords.
    // in RWCard would be used something like
    // position.y = location + self.player.rotationSign * 18.0
    // which will move a card up for P1, and down for P2
    // without putting all of those methods in if blocks
    var rotationSign :CGFloat  {
        return ( zRotation > 1.5 ? -1 : 1)
    }
    var preMovePosition = CGPointZero
    
    // game state
    var currentRegion :RWCardRegion?  // is deck a region
    var active = true
    var attackedThisTurn = false
    var incapacitated = false
    var moved = false
    var attachedCards :[Card] = []
    var discardOnTurn :Int?
    var damage = 0
    
    class func cardName(name: RWCardName) -> Card {
        return Card(cardName: name)
    }
    
    init(cardName named: RWCardName) {
        
        cardName = named
        damageLabel = SKLabelNode(fontNamed: "OpenSans-Bold")
        backTexture = SKTexture(imageNamed: "card_back.png")
        
        switch cardName {
        case .CreatureWolf:
            cardType = "Creature"
            frontTexture = SKTexture(imageNamed: "card_creature_wolf.png")
            highlightTexture = SKTexture(imageNamed: "card_creature_wolf_sel.png")
            largeCardFileName = "card_creature_wolf_large.png"
            energyCost = 1
            cardAttackValue = 1
            cardDefenseValue = 1
            
        case .CreatureBear:
            cardType = "Creature"
            frontTexture = SKTexture(imageNamed: "card_creature_bear.png")
            highlightTexture = SKTexture(imageNamed: "card_creature_bear_sel.png")
            largeCardFileName = "card_creature_bear_large.png"
            energyCost = 2
            cardAttackValue = 2
            cardDefenseValue = 2
            
        case .CreatureDragon:
            cardType = "Creature"
            frontTexture = SKTexture(imageNamed: "card_creature_dragon.png")
            highlightTexture = SKTexture(imageNamed: "card_creature_dragon_sel.png")
            largeCardFileName = "card_creature_dragon_large.png"
            energyCost = 3
            cardAttackValue = 3
            cardDefenseValue = 3
            
        case .Energy:
            cardType = "Energy"
            frontTexture = SKTexture(imageNamed: "card_energy.png")
            highlightTexture = SKTexture(imageNamed: "card_energy.png")
            largeCardFileName = "card_energy_large.png"
            
        case .SpellDeathRay:
            cardType = "Spell"
            spellTargets = "Enemy"
            frontTexture = SKTexture(imageNamed: "card_spell_death_ray.png")
            highlightTexture = SKTexture(imageNamed: "card_spell_death_ray_sel.png")
            largeCardFileName = "card_spell_death_ray_large.png"
            energyCost = 2
            
        case .SpellRabid:
            cardType = "Spell"
            spellTargets = "Friendly"
            frontTexture = SKTexture(imageNamed: "card_spell_rabid.png")
            highlightTexture = SKTexture(imageNamed: "card_spell_rabid_sel.png")
            largeCardFileName = "card_spell_rabid_large.png"
            energyCost = 1
            cardAttackValue = 2
            cardDefenseValue = 2
            spellDuration = 1
            
        case .SpellSleep:
            cardType = "Spell"
            spellTargets = "Enemy"
            frontTexture = SKTexture(imageNamed: "card_spell_sleep.png")
            highlightTexture = SKTexture(imageNamed: "card_spell_sleep_sel.png")
            largeCardFileName = "card_spell_sleep_large.png"
            energyCost = 1
            spellDuration = 1
            
        case .SpellStoneskin:
            cardType = "Spell"
            spellTargets = "Friendly"
            frontTexture = SKTexture(imageNamed: "card_spell_stoneskin.png")
            highlightTexture = SKTexture(imageNamed: "card_spell_stoneskin_sel.png")
            largeCardFileName = "card_spell_stoneskin_large.png"
            energyCost = 1
            cardAttackValue = 0
            cardDefenseValue = 2
            // should use a flag value, but negative number won't work with 
            // turn calculation. Using large number for 'never'
            spellDuration = 1000000
        }
        
        super.init(texture: frontTexture, color: nil, size: frontTexture.size())
        
        damageLabel.fontSize = 12
        damageLabel.fontColor = UIColor.darkRedColor()
        damageLabel.text = "0"
        damageLabel.hidden = true
        damageLabel.position = CGPointMake(25, 40)
        addChild(damageLabel)
        
        userInteractionEnabled = true
    }
    
    func doDamage(amount: Int) {
        damage += amount
        damageLabel.text = "-\(damage)"
        if damage != 0 {
            damageLabel.hidden = false
        }
    }
    
    // #pragma mark - Card Actions
    func faceUp() {
        texture = frontTexture
    }
    
    func faceDown() {
        texture = backTexture
    }
    
    func highlight() {
        texture = highlightTexture
    }
    
    func clearHighlight() {
        texture = frontTexture
    }
    
    func cardHint() {
        switch cardType {
        case "Energy":
            gameScene?.notifyWithString("Drag Energy card to the energy region")
            
        case "Creature":
            gameScene?.notifyWithString("Drag Creature cards to the center of the board to summon")
            
        case "Spell":
            gameScene?.notifyWithString("Drag Spell card onto target to cast")
            
        default:
            println("Invalid card type")
        }
    }
    
    func discard() {
        player?.discard?.addCard(self, fromRegion: currentRegion)
    }
    
    // pragma mark - Attached Cards
    
    func attachCard(attachment: Card) {
        attachment.currentRegion?.removeCard(attachment)
        
        if attachment.cardName == RWCardName.SpellSleep {
            incapacitated = true
        }
        
        attachment.discardOnTurn = gameScene!.turnNumber + attachment.spellDuration
        attachedCards.append(attachment)
        
        let index = find(attachedCards, attachment)
        
        attachment.position = CGPointMake(position.x, position.y + rotationSign * 18.0 * CGFloat(index! + 1))
        attachment.zPosition = zPosition - CGFloat(index! + 1)
        attachment.zRotation = zRotation
        attachment.savedZRotation = zRotation
        attachment.removeActionForKey("rotate")
    }
    
    func haveAttachmentsExpired() {
        var cardsToDiscard :[Card] = []
        for attachment in attachedCards {
            if gameScene?.turnNumber >= attachment.discardOnTurn {
                cardsToDiscard.append(attachment)
            }
        }
        
        for attachment in cardsToDiscard {
            discardAttachedCard(attachment)
        }
        restackAttachments()
    }
    
    func discardAttachedCard(attachemnt: Card) {
        if attachemnt.cardName == RWCardName.SpellSleep {
            incapacitated = false
        }
        
        if let cardIndex = find(attachedCards, attachemnt) {
            attachedCards.removeAtIndex(cardIndex)
        }
        
        if damage >= defenseValue {
            discard()
        }
        
    }
    
    func discardAllAttachments() {
        for attachment in attachedCards {
            attachment.discard()
        }
        attachedCards.removeAll(keepCapacity: false)
    }
    
    func restackAttachments() {
        for (index, attachment) in enumerate(attachedCards) {
            let newPosition = CGPointMake(position.x, position.y + rotationSign * 18.0 * CGFloat(index + 1))
            attachment.position = newPosition
        }
    }
    
    func returnToHomePosition() {
        let returnToPreMovePosition = SKAction.moveTo(preMovePosition, duration: 0.3)
        runAction(returnToPreMovePosition) {
            self.zPosition = self.savedZPosition
            self.restackAttachments()
        }
    }
    
    // #pragma mark - Touch Handlers
    
    override func touchesBegan(touches: NSSet!, withEvent event: UIEvent!) {
        let touch = touches.anyObject() as UITouch
        
        savedZRotation = zRotation
        runAction(SKAction.scaleTo(1.5, duration: 0.2))
        moved = false
        
        // hack so dragging card goes on top of other cards
        // set to 19 so its not above the tracker layers (20)
        savedZPosition = zPosition
        zPosition = 19
        preMovePosition = position

        // inactive cards including used energy cards, can't use again
        if !active { return }
        
        if (gameScene?.subPhase == "SelectEnemyTarget") & (player != gameScene?.currentPlayer) {
            gameScene?.castSpellOnTarget(self)
        }
        
        if (gameScene?.subPhase == "SelectFriendlyTarget") & (player == gameScene?.currentPlayer) {
            gameScene?.castSpellOnTarget(self)
        }
        
        // double tap shows large version of the card
        // check to make sure the tapped card belongs to current player - No Peeking!
        // added check for not in EnergyTracker region. Since card movement is automatic in that region
        // tap on card will move it. Double tap in same place will trigger enlarged card on second energy card.
        if (touch.tapCount >= 2) &&
            ((player == gameScene?.currentPlayer) || !(currentRegion?.name == "Hand")) &&
            (currentRegion !== player?.energyTracker) {
                        gameScene?.showLargeCard(self)
        }
        
        // shouldn't activate cards from defending player, exit early
        // if need touches on defenderPlayer cards, put before here
        if gameScene?.currentPlayer != player { return }
        
        if (cardType == "Energy" && currentRegion?.name == "Energy Tracker") {
            player?.energyTracker?.addStarFromCard(self)
        }
    }
    
    override func touchesMoved(touches: NSSet!, withEvent event: UIEvent!) {
        // setup wiggle animation, but only if not active
        if !actionForKey("wiggle") {
            let rotR = SKAction.rotateByAngle(0.15, duration: 0.2)
            let rotL = SKAction.rotateByAngle(-0.15, duration: 0.2)
            let cycle = SKAction.sequence([rotR, rotL, rotL, rotR])
            let wiggle = SKAction.repeatActionForever(cycle)
            runAction(wiggle, withKey: "wiggle")
        }
        
        // shouldn't move cards from defending player, exit early
        if gameScene?.currentPlayer != player {
            return
        }
        
        // in-play energy cards shouldn't move
        // useing === because of Swift - ObjC bridge issues
        if currentRegion === player?.energyTracker {
            return
        }
        
        let touch = touches.anyObject() as UITouch
        let location = touch.locationInNode(scene)
        
        moved = true
        position = location
        
        // also change position for attached cards
        restackAttachments()
    }
    
    override func touchesEnded(touches: NSSet!, withEvent event: UIEvent!) {
        runAction(SKAction.scaleTo(1.0, duration: 0.2))
        removeActionForKey("wiggle")
        runAction(SKAction.rotateToAngle(savedZRotation, duration: 0.2), withKey:"rotate")
        
        // return card to previous zPosition
        // activated energy cards have been reassigned a zposition
        // for stacking issues. Moving card zposition should be 19
        // if zPos is not 19, don't change it
        if zPosition == 19 {
            zPosition = savedZPosition
        }
        
        // energy cards movement is taken care of in EnergyTracker
        if ( (cardType == "Energy") && (currentRegion!.name == "Energy Tracker")) {
            return
        }
        
        if !moved {
            return
        }
        
        let touch = touches.anyObject() as UITouch
        let location = touch.locationInNode(scene)
        
        // not sure what type this is coming across as, think its SKNode! not RWCardRegion
        let dropRegion = gameScene!.regionForPoint(location)
        
        // creature card dragged over player tracker -> attack other player
        // also must already be In Play
        if ( (cardType == "Creature") && gameScene!.playerTracker.containsPoint(position) &&
            (currentRegion!.name == "InPlay")) {
            if incapacitated {
                gameScene!.notifyWithString("Creature cannot attack")
            } else {
                gameScene!.fightWithAttacker(self, andDefender: nil)
            }
            returnToHomePosition()
            return
        }
        
        // if card is dragged to a point that is not part of a drop region
        // return to initial position
        if !dropRegion {
            returnToHomePosition()
            return
        }
        
        // some edge cases, like drop on enemies discard pile
        if !dropRegion!.name {
            returnToHomePosition()
            return
        }
        
        // Card is a creature, is already in play, dropped in an inplay region
        // most likely you want to attack - need to check
        if ( (cardType == "Creature") && (dropRegion!.name == "InPlay") && (currentRegion!.name == "InPlay")) {
            if incapacitated {
                gameScene!.notifyWithString("Creature cannot attack")
                returnToHomePosition()
                return
            }
            
            // might be nil
            if let cardDroppedOn = (dropRegion as RWInPlayRegion).cardAtPoint(position) {
                if cardDroppedOn.cardType == "Creature" && (player != cardDroppedOn.player?) {
                    gameScene!.fightWithAttacker(self, andDefender: cardDroppedOn)
                }
            } else {
                gameScene!.notifyWithString("Drag onto creature of player to attack")
            }
            
            returnToHomePosition()
            return
        }
        
        if ( cardType == "Spell" && dropRegion!.name == "InPlay" && currentRegion!.name == "Hand" ) {
            if (dropRegion as RWCardRegion).shouldAddCard(self, fromRegion: currentRegion) {
                gameScene?.cast(self, onTarget: (dropRegion as RWInPlayRegion).cardAtPoint(position))
                return
            }
        }
        

        
        // card is dropped into the same region
        // maybe should reorder, not in at this point
        if dropRegion === currentRegion {
            // so that animation doesn't slide the card under another card
            // returnToHomePosition() will return to correct zPosition
            self.zPosition = 19
            returnToHomePosition()
            
            if currentRegion!.name == "Hand" {
                cardHint()
            }
            
            return
        }
        
        // dropped in a new region, ask if it can move
        if dropRegion !== currentRegion {
            if (dropRegion as RWCardRegion).shouldAddCard(self, fromRegion: currentRegion) {
                (dropRegion as RWCardRegion).addCard(self, fromRegion: currentRegion)
            } else {
                returnToHomePosition()
            }
        }
    }
    
    override func touchesCancelled(touches: NSSet!, withEvent event: UIEvent!) {
        
    }
}
