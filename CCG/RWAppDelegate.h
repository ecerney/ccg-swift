//
//  RWAppDelegate.h
//  CCG
//
//  Created by Brian Broom on 2/25/14.
//  Copyright (c) 2014 Brian Broom. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RWAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
