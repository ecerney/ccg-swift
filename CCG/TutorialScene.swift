//
//  TutorialScene.swift
//  CCG
//
//  Created by Brian Broom on 7/5/14.
//  Copyright (c) 2014 Brian Broom. All rights reserved.
//

import Foundation
import SpriteKit
import AVFoundation

class TutorialScene : SKScene {
    
    let musicPlayer :AVAudioPlayer
    let images = ["draw.png", "playEnergy.png","changePhase.png", "activateEnergy.png", "castCreatures.png", "attack.png"];
    var index = 0
    var sliding = false
    
    init(size: CGSize) {
        
        // sound setup
        // play background music
        let path = NSBundle.mainBundle().pathForResource("Pippin the Hunchback", ofType: "mp3")
        let musicFile = NSURL(fileURLWithPath: path)
        
        var error :NSError? = nil
        musicPlayer = AVAudioPlayer(contentsOfURL: musicFile, error: &error)
        musicPlayer.numberOfLoops = -1
        musicPlayer.prepareToPlay()
        musicPlayer.play()
        
        super.init(size: size)
        
        // button to return to menu
        let menuButton = SKSpriteNode(imageNamed: "MenuButton.png")
        menuButton.name = "menu"
        menuButton.zPosition = 10
        menuButton.position = CGPointMake(CGRectGetMidX(frame), 40)
        addChild(menuButton)
        
        // first tutorial screen
        let screenOne = SKSpriteNode(imageNamed: images[index])
        screenOne.name = "onScreen"
        screenOne.position = CGPointMake(512, 384)
        
        let screenTwo = SKSpriteNode(imageNamed: images[index+1])
        screenTwo.name = "offScreen"
        screenTwo.position = CGPointMake(1536, 384)
        
        addChild(screenOne)
        addChild(screenTwo)
    }
    
    func nextImage() {
        index++
        
        if (index+1 == images.count) {
            index = 0
        }
        
        let offScreen = childNodeWithName("offScreen")
        offScreen.name = "onScreen"
        
        let newScreen = SKSpriteNode(imageNamed: images[index+1])
        newScreen.name = "offScreen"
        newScreen.position = CGPointMake(1536, 384)
        addChild(newScreen)
    }
    
    override func touchesBegan(touches: NSSet, withEvent event: UIEvent) {
        
        // exit early if still animating
        if (sliding) { return }
        
        // slide image off screen
        sliding = true
        // slide a full screen width to the left
        let slideLeft = SKAction.moveByX(-1024, y: 0, duration: 0.3)
        let remove = SKAction.removeFromParent()
        
        childNodeWithName("onScreen")?.runAction(SKAction.sequence([slideLeft, remove])) {
            self.sliding = false
        }
        childNodeWithName("offScreen")?.runAction(slideLeft)
        
        let touch = touches.anyObject() as UITouch
        let location = touch.locationInNode(self)
        let touchNode = nodeAtPoint(location)
        
        let pushDown = SKAction.scaleTo(0.8, duration: 0.2)
        let click = SKAction.playSoundFileNamed("click.wav", waitForCompletion: true)
        let pushUp = SKAction.scaleTo(1.0, duration: 0.2)
        
        let clickAndUp = SKAction.group([click, pushUp])
        let push = SKAction.sequence([pushDown, clickAndUp])
        
        if touchNode.name == "menu" {
            touchNode.runAction(push) {
                self.musicPlayer.stop()
                let menuScene = MenuScene(size: self.size)
                self.view.presentScene(menuScene)
            }
        }
        
        nextImage()        
    }
    
}