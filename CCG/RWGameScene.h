//
//  RWGameScene.h
//  CCG
//
//  Created by Brian Broom on 2/25/14.
//  Copyright (c) 2014 Brian Broom. All rights reserved.
//

#import <SpriteKit/SpriteKit.h>
#import "RWCardRegion.h"

@class RWCard;
@class Player;
@class RWStarTracker;
@class RWPlayerTracker;

@interface RWGameScene : SKScene

@property (copy, nonatomic) NSString *currentPhase;
@property (copy, nonatomic) NSString *subPhase;
@property (assign, nonatomic) NSInteger turnNumber;
@property (weak, nonatomic) Player *currentPlayer;
@property (weak, nonatomic) Player *defendingPlayer;
@property (strong, nonatomic) Card *spellToCast;
@property (strong, nonatomic) RWStarTracker *starTracker;
@property (strong, nonatomic) RWPlayerTracker *playerTracker;

//- (void)drawCard:(RWCard *)card;

- (void)showLargeCard:(Card *)card;
- (void)notifyWithString:(NSString *)message;
- (SKNode<RWCardRegion> *)regionForPoint:(CGPoint)point;
- (void)selectTargetsForSpell:(Card *)card;
- (void)cast: (Card *)spell onTarget:(Card *)target;
- (void)castSpellOnTarget:(Card *)target;
- (void)fightWithAttacker:(Card *)attacker andDefender:(Card *)defender;
- (void)endFight;
- (void)playerDies:(Player *)player;
- (void)nextTurn;

@end
