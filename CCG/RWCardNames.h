//
//  RWConstants.h
//  CCG
//
//  Created by Brian Broom on 1/29/14.
//  Copyright (c) 2014 Brian Broom. All rights reserved.
//


typedef NS_ENUM(NSInteger, RWCardName) {
    CreatureWolf,       // 0
    CreatureBear,       // 1
    CreatureDragon,     // 2
    Energy,             // 3
    SpellDeathRay,      // 4
    SpellRabid,         // 5
    SpellSleep,         // 6
    SpellStoneskin      // 7
};