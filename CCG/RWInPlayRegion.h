//
//  RWInPlayRegion.h
//  CCG
//
//  Created by Brian Broom on 2/27/14.
//  Copyright (c) 2014 Brian Broom. All rights reserved.
//

#import <SpriteKit/SpriteKit.h>
#import "RWCardRegion.h"

@class Player;
@class Card;

@interface RWInPlayRegion : SKSpriteNode <RWCardRegion>

@property (weak, nonatomic) Player *player;

- (NSArray *)creatures;
- (NSArray *)fighters;
- (void)clearHighlights;
- (void)resetAttacks;
- (void)checkCreaturesForExpiredAttachments;
- (Card *)cardAtPoint:(CGPoint)point;
//- (BOOL)canAttackAtPoint:(CGPoint)point;
- (void)attackCardAtPoint:(CGPoint)point withCard:(Card *)attacker;

@end
