//
//  TextOverlay.swift
//  CCG
//
//  Created by Brian Broom on 7/6/14.
//  Copyright (c) 2014 Brian Broom. All rights reserved.
//

import Foundation
import SpriteKit

class TextOverlay : SKSpriteNode {
    
    let textLabel :MultilineLabelNode
    
    init(color: UIColor, size: CGSize) {

        textLabel = MultilineLabelNode(fontNamed: "OpenSans")
        textLabel.fontSize = 50
        textLabel.text = " "
        textLabel.fontColor = UIColor.blackColor()
        textLabel.name = "text"
        
        super.init(texture: nil, color: UIColor.regionBackgroundColor(), size: size)

        addChild(textLabel)
        
        zPosition = -20
        userInteractionEnabled = true
        
        let overlayLabel = SKLabelNode(fontNamed: "OpenSans")
        overlayLabel.fontSize = 40
        overlayLabel.fontColor = UIColor.blackColor()
        overlayLabel.position = CGPointMake(0, -300)
        overlayLabel.text = "Tap to dismiss."
        addChild(overlayLabel)
    }
 
    func notify(#string: String, andRotation rotation: CGFloat) {
        zPosition = 20
        zRotation = rotation
        textLabel.text = string
    }
    
    func dismiss() {
        textLabel.text = " "
        zPosition = -20
    }
    
    override func touchesBegan(touches: NSSet, withEvent event: UIEvent) {
        dismiss()
    }
}