//
//  LargeCardOverlay.swift
//  CCG
//
//  Created by Brian Broom on 7/6/14.
//  Copyright (c) 2014 Brian Broom. All rights reserved.
//

import Foundation
import SpriteKit

class LargeCardOverlay : SKSpriteNode {
    
    var largeCard :SKSpriteNode?
    var smallCard :Card?
    
    init(color: UIColor, size: CGSize) {
        super.init(texture: nil, color: UIColor.regionBackgroundColor(), size: size)
        
        zPosition = -20
        userInteractionEnabled = true
    }

    func displayLargeCard(card: Card, andRotation rotation: CGFloat) {
        // clean up from previous
        removeAllChildren()

        smallCard = card
        zRotation = rotation
        largeCard = SKSpriteNode(imageNamed: card.largeCardFileName)
        
        // scale and position of small card
        largeCard!.setScale(0.364)
        largeCard!.position = convertPoint(card.position, fromNode: card.scene)
        addChild(largeCard)
        
        // card setup, now show
        zPosition = 20
        
        let moveToCenter = SKAction.moveTo(CGPointZero, duration: 0.3)
        let enlarge = SKAction.scaleTo(2.0, duration: 0.3)
        
        largeCard!.runAction(SKAction.group([moveToCenter, enlarge]))
    }
    
    func dismiss() {
        if smallCard {
            let oldPosition = convertPoint(smallCard!.position, fromNode: smallCard!.scene)
            
            let moveBack = SKAction.moveTo(oldPosition, duration: 0.3)
            let shrink = SKAction.scaleTo(0.364, duration: 0.3)
            
            largeCard!.runAction(SKAction.group([moveBack, shrink])) {
                self.smallCard = nil
                self.largeCard = nil
                self.zPosition = -20
            }
        }
    }
    
    override func touchesBegan(touches: NSSet, withEvent event: UIEvent) {
        dismiss()
    }
}