//
//  RWHand.h
//  CCG
//
//  Created by Brian Broom on 2/27/14.
//  Copyright (c) 2014 Brian Broom. All rights reserved.
//

#import <SpriteKit/SpriteKit.h>
#import "RWCardRegion.h"

@class Player;

@interface RWHand : SKSpriteNode <RWCardRegion>

@property (weak, nonatomic) Player *player;

- (void)faceDown;
- (void)faceUp;
- (void)recalculateCardPositions;

@end
