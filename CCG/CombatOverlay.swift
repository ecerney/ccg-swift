//
//  CombatOverlay.swift
//  CCG
//
//  Created by Brian Broom on 7/6/14.
//  Copyright (c) 2014 Brian Broom. All rights reserved.
//

import Foundation
import SpriteKit

class CombatOverlay : SKSpriteNode {
    
    var originalAttacker :Card?
    var originalDefender :Card?
    var attackerNode :SKSpriteNode?
    var defenderNode :SKSpriteNode?
    var step :Int = 0
    
    init(color: UIColor, size: CGSize) {
        super.init(texture: nil, color: UIColor.regionBackgroundColor(), size: size)
        
        zPosition = -20
        userInteractionEnabled = true
    }
    
    func showFightWithAttacker(attacker: Card, andDefender defender:Card?, andRotation rotation:CGFloat) {
        // defender might be nil (attacking the player)
        step = 1
        zPosition = 20
        zRotation = rotation
        
        originalAttacker = attacker
        originalDefender = defender
        
        attackerNode = SKSpriteNode(imageNamed: attacker.largeCardFileName)
        attackerNode!.position = CGPointMake(-171, 0)
        addChild(attackerNode!)
        
        if defender {
            defenderNode = SKSpriteNode(imageNamed: defender!.largeCardFileName)
            defenderNode!.position = CGPointMake(171, 0)
            addChild(defenderNode)
        } else {
            // no defender, attacking the player. Show a representation of the player
            var player :SKSpriteNode
            let gameScene = scene as RWGameScene
            
            switch gameScene.defendingPlayer.name! {
            case "Druid":
                player = SKSpriteNode(imageNamed: "player1_druid.png")
            case "Beastmaster":
                player = SKSpriteNode(imageNamed: "player2_beastmaster.png")
            default:
                player = SKSpriteNode(imageNamed: "player1_druid.png")
            }
            
            player.position = CGPointMake(171, 60)
            addChild(player)
            
            let heart = SKSpriteNode(imageNamed: "icon_heart.png")
            heart.position = CGPointMake(144, 10)
            addChild(heart)
            
            let healthLabel = SKLabelNode(fontNamed: "OpenSans-Bold")
            healthLabel.name = "health"
            healthLabel.fontSize = 36
            healthLabel.fontColor = UIColor.blackColor()
            healthLabel.position = CGPointMake(190, -5)
            let health = gameScene.playerTracker.healthForPlayer(gameScene.defendingPlayer)
            healthLabel.text = "\(health)"
            addChild(healthLabel)
        }
        
        // damage overlay
        // add damage overlay, mimicing what will show on the actual card
        
        let attackerDamageOverlay = SKLabelNode(fontNamed: "OpenSans-Bold")
        attackerDamageOverlay.fontSize = 24
        attackerDamageOverlay.name = "AttackerDamageOverlay"
        attackerDamageOverlay.fontColor = UIColor.darkRedColor()
        attackerDamageOverlay.text = "-\(originalAttacker!.damage)"
        
        if (originalAttacker!.damage == 0) {
            attackerDamageOverlay.hidden = true
        }
        
        attackerDamageOverlay.position = CGPointMake(-105, 121)
        addChild(attackerDamageOverlay)
        
        if originalDefender {
            let defenderDamageOverlay = SKLabelNode(fontNamed: "OpenSans-Bold")
            defenderDamageOverlay.fontSize = 24
            defenderDamageOverlay.name = "DefenderDamageOverlay"
            defenderDamageOverlay.fontColor = UIColor.darkRedColor()
            defenderDamageOverlay.text = "-\(originalDefender!.damage)"
            
            if originalDefender!.damage == 0 {
                defenderDamageOverlay.hidden = true
            }
            
            defenderDamageOverlay.position = CGPointMake(236, 121)
            addChild(defenderDamageOverlay)
        }
        
        let delay = SKAction.waitForDuration(0.1)
        //let next = SKAction.runBlock(assignDamage)
        //let waitRunNext = SKAction.sequence([delay, next])
        //runAction(waitRunNext, withKey: "delay")
        runAction(delay) {
            self.assignDamage()
        }
    }
    
    func assignDamage() {
        step = 2
        
        let driftUp = SKAction.moveByX(0.0, y: 50.0, duration: 0.5)
        let fadeOut = SKAction.fadeOutWithDuration(0.5)
        let fadeUp = SKAction.group([driftUp, fadeOut])
        
        if originalDefender {
            // attacker looses health, like in MMO's
            let attackerDamage = SKLabelNode(fontNamed: "OpenSans-Regular")
            attackerDamage.fontSize = 40
            attackerDamage.fontColor = UIColor.darkRedColor()
            attackerDamage.text = "-\(originalDefender!.attackValue)"
            attackerDamage.position = CGPointMake(-171, 250)
            addChild(attackerDamage)
            attackerDamage.runAction(fadeUp)
            
            // update attacker damage overlay
            let attackerDamageOverlay = childNodeWithName("AttackerDamageOverlay") as SKLabelNode
            let attackerTotalDamage = originalAttacker!.damage + originalDefender!.attackValue
            attackerDamageOverlay.text = "-\(attackerTotalDamage)"
            if attackerTotalDamage > 0 {
                attackerDamageOverlay.hidden = false
            }
        }
        
        // defender loss of health
        let defenderDamage = SKLabelNode(fontNamed: "OpenSans-Regular")
        defenderDamage.fontSize = 40
        defenderDamage.fontColor = UIColor.darkRedColor()
        defenderDamage.text = "-\(originalAttacker!.attackValue)"
        defenderDamage.position = CGPointMake(171, 250)
        addChild(defenderDamage)
        defenderDamage.runAction(fadeUp)
        
        // update defender damage overlay
        if originalDefender {
            let defenderDamageOverlay = childNodeWithName("DefenderDamageOverlay") as SKLabelNode
            let defenderTotalDamage = originalDefender!.damage + originalAttacker!.attackValue
            defenderDamageOverlay.text = "-\(defenderTotalDamage)"
            if (defenderTotalDamage > 0) {
                defenderDamageOverlay.hidden = false
            }
        }
        
        if defenderNode {
            runAction(SKAction.playSoundFileNamed("two-swords.wav", waitForCompletion: false))
        } else {
            runAction(SKAction.playSoundFileNamed("one-sword.wav", waitForCompletion: false))
            
            let healthLabel = childNodeWithName("health") as SKLabelNode
            let gameScene = scene as RWGameScene
            let health = gameScene.playerTracker.healthForPlayer(gameScene.defendingPlayer) - originalAttacker!.attackValue
            healthLabel.text = "\(health)"
        }
        
        removeActionForKey("delay")
        let delay = SKAction.waitForDuration(1.0)
        //let next = SKAction.runBlock(didCreaturesDie)
        //let waitNextRun = SKAction.sequence([delay, next])
        //runAction(waitNextRun, withKey: "delay")
        runAction(delay) {
            self.didCreaturesDie()
        }
    }
    
    func didCreaturesDie() {
        step = 3
        var deadCount = 0
        
        if originalDefender {
            originalDefender!.doDamage(originalAttacker!.attackValue)
            originalAttacker!.doDamage(originalDefender!.attackValue)
            
            if originalAttacker!.damage >= originalAttacker!.defenseValue {
                let deadX = SKSpriteNode(imageNamed: "deadCreature.png")
                deadX.position = attackerNode!.position
                addChild(deadX)
                deadCount++
            }
            
            if originalDefender!.damage >= originalDefender!.defenseValue {
                let deadX = SKSpriteNode(imageNamed: "deadCreature.png")
                deadX.position = defenderNode!.position
                addChild(deadX)
                deadCount++
            }
            
            let playSound = SKAction.playSoundFileNamed("thump.wav", waitForCompletion: true)
            runAction(SKAction.repeatAction(playSound, count: deadCount))
        }
        
        removeActionForKey("delay")
        
        if deadCount > 0 {
            let delay = SKAction.waitForDuration(0.5)
            //let next = SKAction.runBlock(dismiss)
            //let waitRunNext = SKAction.sequence([delay, next])
            //runAction(waitRunNext, withKey: "delay")
            runAction(delay) {
                self.dismiss()
            }
        } else {
            nextStep()
        }
    }
    
    func dismiss() {
        attackerNode = nil
        defenderNode = nil
        originalAttacker = nil
        originalDefender = nil
        zPosition = -20
        removeAllChildren()
        (scene as RWGameScene).endFight()
    }
    
    func nextStep() {
        switch step {
        case 1:
            removeActionForKey("delay")
            assignDamage()
        case 2:
            removeActionForKey("delay")
            didCreaturesDie()
        default:  // also for case 3, not a case 4
            removeActionForKey("delay")
            dismiss()
        }
    }
    
    override func touchesBegan(touches: NSSet!, withEvent event: UIEvent!) {
        removeActionForKey("delay")
        nextStep()
    }
    
}