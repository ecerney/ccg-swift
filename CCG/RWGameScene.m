//
//  RWGameScene.m
//  CCG
//
//  Created by Brian Broom on 2/25/14.
//  Copyright (c) 2014 Brian Broom. All rights reserved.
//

@import AVFoundation;

#import "RWGameScene.h"
#import "RWCardNames.h"
//#import "RWDeck.h"
#import "RWHand.h"
#import "RWDiscard.h"
#import "RWPlayerTracker.h"
#import "RWCardRegion.h"
#import "RWEnergyTracker.h"
#import "RWInPlayRegion.h"
#import "RWStarTracker.h"
#import "RWCardRegion.h"
#import "RW CCG-Bridging-Header.h"
#import "RW_CCG-Swift.h"

@interface RWGameScene ()

@property (strong, nonatomic) AVAudioPlayer *musicPlayer;
@property (strong, nonatomic) NSArray *players;
@property (strong, nonatomic) LargeCardOverlay *largeCardOverlay;
@property (strong, nonatomic) TextOverlay *textOverlay;
@property (strong, nonatomic) CombatOverlay *combatOverlay;
@property (strong, nonatomic) NSMutableArray *regions;
@property (weak, nonatomic) Card *attackingCard;
@property (weak, nonatomic) Card *defendingCard;
@property (strong, nonatomic) NSMutableArray *attackers;
@property (strong, nonatomic) NSMutableArray *defenders;
@property (assign, nonatomic) BOOL pausedForNewTurn;
@property (weak, nonatomic) SKNode *currentOverlay;

@end

@implementation RWGameScene

-(id)initWithSize:(CGSize)size {
    if (self = [super initWithSize:size]) {
        
        // play background music
        NSString *path = [[NSBundle mainBundle] pathForResource:@"Black Vortex" ofType:@"mp3"];
        NSURL *musicFile = [[NSURL alloc] initFileURLWithPath:path];
        NSError *error = nil;
        error = nil;
        _musicPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:musicFile  error:&error];
        _musicPlayer.numberOfLoops = -1; // negative value repeats indefinitely
        [_musicPlayer prepareToPlay];
        [_musicPlayer play];
        
        // add game board image
        SKSpriteNode *table = [SKSpriteNode spriteNodeWithImageNamed:@"bg_board.png"];
        table.zPosition = -10;
        table.position = CGPointMake(CGRectGetMidX(self.frame), CGRectGetMidY(self.frame));
        [self addChild:table];
        
        // add star tracker
        
        _starTracker = [RWStarTracker new];
        _starTracker.position = CGPointMake(130, 383);
        [self addChild:_starTracker];
        
        _players = @[[Player new], [Player new]];
        
        _regions = [NSMutableArray new];
        [self setupPlayerOne];
        [self setupPlayerTwo];
        
        // add player tracker
        
        _playerTracker = [RWPlayerTracker new];
        _playerTracker.position = CGPointMake(946, 384);
        _playerTracker.userInteractionEnabled = YES;
        [self addChild:_playerTracker];
        
        
        // add overlays
        
        _largeCardOverlay = [[LargeCardOverlay alloc] initWithColor:[UIColor grayColor] size:self.size];
        _largeCardOverlay.position = CGPointMake(CGRectGetMidX(self.frame), CGRectGetMidY(self.frame));
        [self addChild:_largeCardOverlay];
        
        _textOverlay = [[TextOverlay alloc] initWithColor:[UIColor grayColor] size:self.size];
        _textOverlay.position = CGPointMake(CGRectGetMidX(self.frame), CGRectGetMidY(self.frame));
        [self addChild:_textOverlay];
        
        _combatOverlay = [[CombatOverlay alloc] initWithColor:[UIColor grayColor] size:self.size];
        _combatOverlay.position = CGPointMake(CGRectGetMidX(self.frame), CGRectGetMidY(self.frame));
        [self addChild:_combatOverlay];
        
        // Turn and Phase Setup
        
        _turnNumber = 0;
        _currentPhase = @"Draw";
        _currentPlayer = _players[ _turnNumber % 2 ];
        _defendingPlayer = _players[ (_turnNumber + 1) % 2];
        
        [_currentPlayer makeActive];
        [_defendingPlayer makeInactive];
        
        // check NSUserDefaults for flag to display basic instructions
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        BOOL hasShownMiniTutorial = NO; //[defaults boolForKey:@"hasShownMiniTutorial"];
        if (!hasShownMiniTutorial) {
            //NSLog(@"Has NOT shown tutorial");
            [self notifyWithString:@"This is a game for 2 players. \n Drag creatures onto creatures or the player image to attack. \n Drag spells onto creatures to attack. \n Tap the screen to unpause between turns."];
            [defaults setBool:YES forKey:@"hasShownMiniTutorial"];
            [defaults synchronize];
        }
        
        self.pausedForNewTurn = YES;
        SKAction *slideLeft = [SKAction moveToX:512 duration:0.5];
        [self.currentPlayer.turnNotifyOverlay runAction:slideLeft];
    }
    return self;
}

- (void)setupPlayerOne {
    Player *p1 = self.players[0];
    p1.name = @"Druid";
    p1.rotation = 0;
    
    NSError *error = nil;
    NSData *data = [NSData dataWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"Decks" ofType:@"json"]];
    NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
    
    NSArray *playerOneCards = json[@"PlayerOne"];
    
    Deck *deck = [[Deck alloc] initWithDataArray:playerOneCards];
    deck.position = CGPointMake(790, 82);
    deck.player = p1;
    p1.deck = deck;
    [self addChild:deck];
    
    RWHand *hand = [RWHand new];
    hand.position = CGPointMake(326, 82);
    hand.player = p1;
    p1.hand = hand;
    [self addChild:hand];
    
    RWDiscard *discard = [RWDiscard new];
    discard.position = CGPointMake(930, 82);
    discard.player = p1;
    p1.discard = discard;
    [self addChild:discard];
    
    RWEnergyTracker *energy = [RWEnergyTracker new];
    energy.position = CGPointMake(130, 270);
    energy.player = p1;
    p1.energyTracker = energy;
    energy.starTracker = self.starTracker;
    [self addChild:energy];
    
    RWInPlayRegion *inPlay = [RWInPlayRegion new];
    inPlay.name = @"InPlay";
    inPlay.zPosition = -10;
    inPlay.position = CGPointMake(560, 279);
    inPlay.player = p1;
    p1.inPlay = inPlay;
    [self addChild:inPlay];
    
    [self.regions addObjectsFromArray:@[p1.hand, p1.discard, p1.inPlay]];
    
    p1.turnNotifyOverlay = [SKNode node];
    p1.turnNotifyOverlay.zPosition = 20;
    p1.turnNotifyOverlay.position = CGPointMake(1536, 384);
    
    SKLabelNode *playerName = [SKLabelNode labelNodeWithFontNamed:@"OpenSans-Bold"];
    playerName.fontSize = 40;
    playerName.fontColor = [UIColor whiteColor];
    playerName.position = CGPointMake(50, 0);
    playerName.text = p1.name;
    [p1.turnNotifyOverlay addChild:playerName];
    
    SKSpriteNode *playerImage = [SKSpriteNode spriteNodeWithImageNamed:@"player1_druid.png"];
    playerImage.position = CGPointMake(-100, 0);
    [p1.turnNotifyOverlay addChild:playerImage];
    
    [self addChild:p1.turnNotifyOverlay];
}

- (void)setupPlayerTwo {
    Player *p2 = self.players[1];
    p2.name = @"Beastmaster";
    p2.rotation = M_PI;
    
    NSError *error = nil;
    NSData *data = [NSData dataWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"Decks" ofType:@"json"]];
    NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
    
    NSArray *playerTwoCards = json[@"PlayerTwo"];
    
    Deck *deck = [[Deck alloc] initWithDataArray:playerTwoCards];
    deck.position = CGPointMake(231, 686);
    deck.zRotation = M_PI;
    deck.player = p2;
    p2.deck = deck;
    [self addChild:deck];
    
    RWHand *hand = [RWHand new];
    hand.position = CGPointMake(702, 686);
    hand.player = p2;
    p2.hand = hand;
    [self addChild:hand];
    
    RWDiscard *discard = [RWDiscard new];
    discard.position = CGPointMake(91, 686);
    discard.zRotation = M_PI;
    discard.player = p2;
    p2.discard = discard;
    [self addChild:discard];
    
    RWEnergyTracker *energy = [RWEnergyTracker new];
    energy.position = CGPointMake(130, 495);
    energy.player = p2;
    p2.energyTracker = energy;
    energy.starTracker = self.starTracker;
    [self addChild:energy];
    
    RWInPlayRegion *inPlay = [RWInPlayRegion new];
    inPlay.name = @"InPlay";
    inPlay.zPosition = -10;
    inPlay.position = CGPointMake(560, 490);
    inPlay.player = p2;
    p2.inPlay = inPlay;
    [self addChild:inPlay];
    
    [self.regions addObjectsFromArray:@[p2.hand, p2.discard, p2.inPlay]];

    p2.turnNotifyOverlay = [SKNode node];
    p2.turnNotifyOverlay.zPosition = 20;
    p2.turnNotifyOverlay.position = CGPointMake(1536, 384);
    p2.turnNotifyOverlay.zRotation = p2.rotation;
    
    SKLabelNode *playerName = [SKLabelNode labelNodeWithFontNamed:@"OpenSans-Bold"];
    playerName.fontSize = 40;
    playerName.fontColor = [UIColor whiteColor];
    playerName.position = CGPointMake(20, 0);
    playerName.text = p2.name;
    [p2.turnNotifyOverlay addChild:playerName];
    
    SKSpriteNode *playerImage = [SKSpriteNode spriteNodeWithImageNamed:@"player2_beastmaster.png"];
    playerImage.position = CGPointMake(-200, 0);
    [p2.turnNotifyOverlay addChild:playerImage];
    
    [self addChild:p2.turnNotifyOverlay];
}

#pragma mark - Turn Change

- (void)nextTurn {
    self.subPhase = @"";
    
    // if spell was being cast, but no target selected, discard the card
    [self.spellToCast discard];
    self.spellToCast = nil;
    
    // clean up end of turn, player ending turn is still currentPlayer
    [self.currentPlayer.inPlay checkCreaturesForExpiredAttachments];
    [self.defendingPlayer.inPlay checkCreaturesForExpiredAttachments];
    [self.currentPlayer.hand recalculateCardPositions];
    [self.currentPlayer.hand faceDown];
    [self.currentPlayer.inPlay clearHighlights];
    
    [self.currentPlayer makeInactive];
    [self.defendingPlayer makeActive];
    
    self.turnNumber += 1;
    
    // in case player didn't make any actions and the overlay is still in place
    // ask it to slide back. If its already there it just won't move.
    SKAction *slideRight = [SKAction moveToX:1536 duration:0.25];
    [self.currentPlayer.turnNotifyOverlay runAction:slideRight];
    
    if (self.turnNumber == 1) {
        [self notifyWithString:@"Game is paused between turns so you can hand off to the other player. \nTap the screen again to unpause."];
    }
    
    self.currentPlayer = self.players[ _turnNumber % 2 ];
    self.defendingPlayer = self.players[ (_turnNumber + 1) % 2];
    //NSLog(@"Current player is now %@", self.currentPlayer.name);
    
    // setup new turn, next player is now currentPlayer
    [self.currentPlayer.energyTracker resetCards];
    [self.currentPlayer.inPlay resetAttacks];
    self.pausedForNewTurn = YES;

    SKAction *slideLeft = [SKAction moveToX:512 duration:0.5];
    [self.currentPlayer.turnNotifyOverlay runAction:slideLeft];
    
    //[self startOfTurn];
}

- (void)startOfTurn {
    SKAction *slideRight = [SKAction moveToX:1536 duration:0.25];
    [self.currentPlayer.turnNotifyOverlay runAction:slideRight];
    [self.currentPlayer.hand faceUp];
    
    Card *drawnCard = [self.currentPlayer.deck drawCard];
    if (!drawnCard) { return; }
    
    if ([drawnCard.cardType isEqualToString:@"Energy"]) {
        [self.currentPlayer.energyTracker addCard:drawnCard fromRegion:nil];
    } else {
        [self.currentPlayer.hand addCard:drawnCard fromRegion:nil];
    }
    
}

#pragma mark - Card Transfer Between Regions

- (id<RWCardRegion>)regionForPoint:(CGPoint)point {
    for (SKNode<RWCardRegion> *region in self.regions) {
        if ([region containsPoint:point]) {
            return region;
        }
    }
    return nil;
}

#pragma mark - Spell Cast

- (void)selectTargetsForSpell:(Card *)card {
    self.spellToCast = card;
    if ([card.spellTargets isEqualToString:@"Friendly"]) {
        self.subPhase = @"SelectFriendlyTarget";
        [self notifyWithString:@"Select a Friendly Target for this spell."];
    }
    
    if ([card.spellTargets isEqualToString:@"Enemy"]) {
        self.subPhase = @"SelectEnemyTarget";
        [self notifyWithString:@"Select an Enemy Target for this spell."];
    }
}


- (void)castSpellOnTarget:(Card *)target {
    [self cast:self.spellToCast onTarget:target];
}


- (void)cast: (Card *)spell onTarget:(Card *)target {
    //NSLog(@"Cast %@ on %@", self.spellToCast.cardType, target.cardType);
    
    switch (spell.cardName) {
        case SpellDeathRay:
            //NSLog(@"Cast death ray");
            [self runAction:[SKAction playSoundFileNamed:@"zoup.wav" waitForCompletion:NO]];
            [spell discard];
            [target discard];
            break;
            
        case SpellRabid:
            //NSLog(@"Cast Rabid");
            [self runAction:[SKAction playSoundFileNamed:@"zoup.wav" waitForCompletion:NO]];
            [target attachCard:spell];
            break;
            
        case SpellSleep:
            //NSLog(@"Cast Sleep");
            [self runAction:[SKAction playSoundFileNamed:@"zoup.wav" waitForCompletion:NO]];
            [target attachCard:spell];
            break;
            
        case SpellStoneskin:
            //NSLog(@"Cast Stoneskin");
            [self runAction:[SKAction playSoundFileNamed:@"zoup.wav" waitForCompletion:NO]];
            [target attachCard:spell];
            break;
            
        default:
            break;
    }
    
    self.subPhase = @"";
    self.spellToCast = nil;
}

#pragma mark - Combat Methods

- (void)fightWithAttacker:(Card *)attacker andDefender:(Card *)defender {
    //NSLog(@"Fight");
    if (attacker.attackedThisTurn) {
        [self notifyWithString:@"Can only attack once per turn."];
        return;
    }
    self.attackingCard = attacker;
    self.defendingCard = defender;
    [self.combatOverlay showFightWithAttacker:attacker andDefender:defender andRotation:self.currentPlayer.rotation];
}

- (void)endFight {
    // if defendingCard is nil, there was no defender, damage goes to player
    // otherwise creatures damage each other
    if (!self.defendingCard) {
        [self.playerTracker doDamage:self.attackingCard.attackValue toPlayer:self.defendingPlayer];
    } else {
        
        if (self.attackingCard.damage >= self.attackingCard.defenseValue) {
            [self.attackingCard discard];
            //deadCount++;
        }
        
        if (self.defendingCard.damage >= self.defendingCard.defenseValue) {
            [self.defendingCard discard];
            //deadCount++;
        }
    }
    
    self.attackingCard.attackedThisTurn = YES;
    self.defendingCard.attackedThisTurn = YES;
    
    self.attackingCard = nil;
    self.defendingCard = nil;
    
//    if ([self.attackers count] > 0) {
//        [self selectAttacker];
//    }
}

- (void)playerDies:(Player *)player {
    [self notifyWithString:[NSString stringWithFormat:@"%@ Wins!", self.currentPlayer.name]];
    [self runAction:[SKAction playSoundFileNamed:@"fanfare.wav" waitForCompletion:YES] completion:^{
        [self returnToMenu];
    }];
}

- (void)returnToMenu {
    [self.musicPlayer stop];
    SKScene *menuScene = [[MenuScene alloc] initWithSize:self.size];
    [self.view presentScene:menuScene];
}

#pragma mark - Touch Handlers
// These are the touch handlers for touches that are not handled
// by the main game elements.
// Cards, Decks, Energy Cards, Player Turn
// Tracker are all handled by those classes.

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
//    NSLog(@"touch reached board level");
//    CGPoint location = [[touches anyObject] locationInNode:self];
//    NSArray *nodes = [self nodesAtPoint:location];
//    
//    for (SKNode *n in nodes) {
//        NSLog(@"Node %@ at zPosition %f", n, n.zPosition);
//    }
    
    if (self.pausedForNewTurn) {
        self.pausedForNewTurn = NO;
        [self startOfTurn];
    }
}

- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event {
    
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
    
}

- (void)touchesCancelled:(NSSet *)touches withEvent:(UIEvent *)event {
    
}

#pragma mark - Overlay Access Methods

- (void)showLargeCard:(Card *)card {
    [self.largeCardOverlay displayLargeCard:card andRotation:self.currentPlayer.rotation];
}

- (void)notifyWithString:(NSString *)message {
    [self.textOverlay notifyWithString:message andRotation:self.currentPlayer.rotation];
}

@end
