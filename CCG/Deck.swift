//
//  Deck.swift
//  CCG
//
//  Created by Brian Broom on 7/10/14.
//  Copyright (c) 2014 Brian Broom. All rights reserved.
//

import Foundation
import SpriteKit

class Deck :SKSpriteNode {
    
    var active = true
    var hasDrawnThisTurn = false
    weak var player :Player?
    var cards :[Card] = []
    let cardBack :SKTexture
    let highlightCardBack :SKTexture
    weak var gameScene :RWGameScene? {
        return scene as? RWGameScene
    }
    
    init(dataArray: [NSNumber]) {
        cardBack = SKTexture(imageNamed: "card_back.png")
        highlightCardBack = SKTexture(imageNamed: "card_back_sel.png")
        
        super.init(texture: cardBack, color: nil, size: cardBack.size())
        
        for data in dataArray {
            let cardNumber = data.integerValue
            let newCard = Card(cardName: RWCardName.fromRaw(cardNumber)!)
            cards.append(newCard)
        }
        
        var shuffleCount = 0
        do {
            shuffle()
            shuffleCount++
        } while (shuffleCount < 20) || (isBadShuffle())
        
        //println("shuffled \(shuffleCount) times")
        //energyProbability()
        
        // not needed with auto card draw
        //userInteractionEnabled = true
    }
    
    // #pragma  mark - Deck Mechanics
    func shuffle() {
        // from http://nshipster.com/random/
        // see also http://en.wikipedia.org/wiki/Fisher–Yates_shuffle
        let count = cards.count
        
        if count > 1 {
            for var i=count-1; i>0; --i {
                let j=Int( arc4random_uniform( UInt32( i+1 ) ) )
                
                let swapCard = cards.removeAtIndex(i)
                cards.insert(swapCard, atIndex: j)
            }
        }
        //println(cards.count)
    }
    
    func drawCard() -> Card? {
        var drawnCard :Card? = nil
        if cards.count > 0 {
            drawnCard = cards.removeAtIndex(0)
            drawnCard!.zRotation = zRotation
            drawnCard!.position = position
            drawnCard!.player = player
            
            runAction(SKAction.playSoundFileNamed("deal.wav", waitForCompletion: false))
            gameScene?.addChild(drawnCard)
        }
        
        if cards.count == 0 {
            texture = SKTexture(imageNamed: "clear.png")
        }
        
        return drawnCard
    }
    
    func energyProbability() {
        var goodCount = 0
        for var i=0; i<1000; i++ {
            if isBadShuffle() {
                goodCount++
            }
        }
        println(" \(goodCount) bad shuffles out of 1000 runs: \(Double(goodCount)*100.0/1000.0) percent")
    }
    
    func isBadShuffle() -> Bool {
        var badShuffle = true
        
        for var i=0; i<3; i++ {
            if cards[i].cardType == "Energy" {
                badShuffle = false
            }
        }
        
        //if !badShuffle { println("bad shuffle") }
        return badShuffle
    }
}
